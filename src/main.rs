#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use embassy_executor::Spawner;
use embassy_time::Timer;
use esp_backtrace as _;
use esp_hal::{
    clock::ClockControl, embassy, gpio::IO, i2c::I2C, peripherals::Peripherals, prelude::*,
    timer::TimerGroup,
};

#[main]
async fn main(_spawner: Spawner) {
    esp_println::logger::init_logger_from_env();

    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::boot_defaults(system.clock_control).freeze();
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    // Initialize embassy
    let timer_group = TimerGroup::new(peripherals.TIMG0, &clocks);
    embassy::init(&clocks, timer_group);

    let _i2c = I2C::new(
        peripherals.I2C0,
        io.pins.gpio21,
        io.pins.gpio22,
        100u32.kHz(),
        &clocks,
    );

    loop {
        Timer::after_secs(1).await;
    }
}
